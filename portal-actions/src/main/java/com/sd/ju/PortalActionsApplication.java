package com.sd.ju;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PortalActionsApplication {

    public static void main(String[] args) {
        SpringApplication.run(PortalActionsApplication.class);
    }
}
