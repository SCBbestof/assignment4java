package com.sd.ju.soap;

import com.sd.ju.wsdl.GetAllPackagesForUsernameRequest;
import com.sd.ju.wsdl.GetAllPackagesForUsernameResponse;
import com.sd.ju.wsdl.GetAllPackagesRequest;
import com.sd.ju.wsdl.GetAllPackagesResponse;
import com.sd.ju.wsdl.SavePackageRequest;
import com.sd.ju.repository.PackageRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class PackageRepositoryEndpoint {
    private static final String NAMESPACE_URI = "http://sd-project.com/soap";

    private final PackageRepository packageRepository;

    @Autowired
    public PackageRepositoryEndpoint(PackageRepository packageRepository) {
        this.packageRepository = packageRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllPackagesRequest")
    @ResponsePayload
    public GetAllPackagesResponse getAllPackages(@RequestPayload GetAllPackagesRequest request) {
        GetAllPackagesResponse response = new GetAllPackagesResponse();

        response.getPackages().addAll(packageRepository.findAll());

        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllPackagesForUsernameRequest")
    @ResponsePayload
    public GetAllPackagesForUsernameResponse getAllPackagesForUsername(@RequestPayload GetAllPackagesForUsernameRequest request) {
        GetAllPackagesForUsernameResponse response = new GetAllPackagesForUsernameResponse();

        response.getPackages().addAll(packageRepository.findBySenderName(request.getUsername()));
        response.getPackages().addAll(packageRepository.findByReceiverName(request.getUsername()));

        return response;
    }
}
