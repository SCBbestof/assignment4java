package com.sd.ju.soap;

import com.sd.ju.repository.PackageRepository;
import com.sd.ju.wsdl.GetAllPackagesForUsernameRequest;
import com.sd.ju.wsdl.GetAllPackagesForUsernameResponse;
import com.sd.ju.wsdl.GetAllPackagesRequest;
import com.sd.ju.wsdl.GetAllPackagesResponse;
import com.sd.ju.wsdl.GetRoleRequest;
import com.sd.ju.wsdl.GetRoleResponse;
import com.sd.ju.wsdl.GetUserRequest;
import com.sd.ju.wsdl.GetUserResponse;
import com.sd.ju.wsdl.SaveUserRequest;
import com.sd.ju.repository.RoleRepository;
import com.sd.ju.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class UserRepositoryEndpoint {

    private static final String NAMESPACE_URI = "http://sd-project.com/soap";

    private final UserRepository userRepository;

    private final RoleRepository roleRepository;

    @Autowired
    public UserRepositoryEndpoint(UserRepository userRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getUserRequest")
    @ResponsePayload
    public GetUserResponse getUser(@RequestPayload GetUserRequest request) {
        GetUserResponse response = new GetUserResponse();
        response.setUser(userRepository.findByUsername(request.getUsername()));

        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getRoleRequest")
    @ResponsePayload
    public GetRoleResponse getRole(@RequestPayload GetRoleRequest request) {
        GetRoleResponse response = new GetRoleResponse();
        response.setRole(roleRepository.findOne(request.getRole()));

        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "saveUserRequest")
    public void saveUser(@RequestPayload SaveUserRequest request) {
        userRepository.save(request.getUser());
    }
}
