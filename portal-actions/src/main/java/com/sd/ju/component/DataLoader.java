package com.sd.ju.component;

import com.sd.ju.repository.RoleRepository;
import com.sd.ju.repository.UserRepository;
import com.sd.ju.wsdl.Role;
import com.sd.ju.wsdl.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class DataLoader implements ApplicationRunner {

    private final UserRepository userRepository;

    private final RoleRepository roleRepository;

    @Autowired
    public DataLoader(UserRepository userRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    public void run(ApplicationArguments applicationArguments) throws Exception {
        // Add roles
        Role adminRole = new Role();
        Role userRole = new Role();

        adminRole.setId("ROLE_ADMIN");
        userRole.setId("ROLE_USER");

        roleRepository.save(adminRole);
        roleRepository.save(userRole);

        // Add admin user
        User admin = new User();

        admin.setEmail("admin@email.com");
        admin.setUsername("admin");
        admin.setPassword("$2a$06$IESU22//wfOJN4CEYSnZWOwZZ1ERt.3OeOI0T8UIEmNGIIvq6mlvy");
        admin.setEnabled(true);
        admin.getRoles().add(adminRole);

        userRepository.save(admin);
    }
}
