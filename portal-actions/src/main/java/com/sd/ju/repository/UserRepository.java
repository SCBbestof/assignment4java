package com.sd.ju.repository;

import com.sd.ju.wsdl.User;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

/**
 * Spring data repository extension for the {@link User} model.
 */
public interface UserRepository extends MongoRepository<User, String> {

    User findByUsername(final String username);

    List<User> findByUsernameAndPassword(final String username, final String password);

    List<User> findByPasswordAndUsername(final String password, final String username);

    List<User> findByUsernameLike(final String username);

    @Query("{ 'username' : ?0, 'password' : ?1 }")
    List<User> findByUsernameAndPasswordQuery(final String username, final String password);

    User findByEmail(final String email);
}
