package com.sd.ju.repository;

import com.sd.ju.wsdl.Role;

import org.springframework.data.mongodb.repository.MongoRepository;


/**
 * Spring data repository extension for the {@link Role} model.
 */
public interface RoleRepository extends MongoRepository<Role, String> {
}
