package com.sd.ju.repository;

import com.sd.ju.wsdl.Package;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface PackageRepository extends MongoRepository<Package, String> {

    List<Package> findBySenderName(final String senderName);

    List<Package> findByReceiverName(final String receiverName);
}
