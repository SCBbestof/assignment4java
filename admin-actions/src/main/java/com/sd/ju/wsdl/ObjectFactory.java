//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.01.11 at 01:35:34 AM EET 
//


package com.sd.ju.wsdl;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.sd.ju.wsdl package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.sd.ju.wsdl
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetUserRequest }
     * 
     */
    public GetUserRequest createGetUserRequest() {
        return new GetUserRequest();
    }

    /**
     * Create an instance of {@link GetUserResponse }
     * 
     */
    public GetUserResponse createGetUserResponse() {
        return new GetUserResponse();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link GetRoleRequest }
     * 
     */
    public GetRoleRequest createGetRoleRequest() {
        return new GetRoleRequest();
    }

    /**
     * Create an instance of {@link GetRoleResponse }
     * 
     */
    public GetRoleResponse createGetRoleResponse() {
        return new GetRoleResponse();
    }

    /**
     * Create an instance of {@link Role }
     * 
     */
    public Role createRole() {
        return new Role();
    }

    /**
     * Create an instance of {@link SaveUserRequest }
     * 
     */
    public SaveUserRequest createSaveUserRequest() {
        return new SaveUserRequest();
    }

    /**
     * Create an instance of {@link GetPackageRequest }
     * 
     */
    public GetPackageRequest createGetPackageRequest() {
        return new GetPackageRequest();
    }

    /**
     * Create an instance of {@link GetPackageResponse }
     * 
     */
    public GetPackageResponse createGetPackageResponse() {
        return new GetPackageResponse();
    }

    /**
     * Create an instance of {@link Package }
     * 
     */
    public Package createPackage() {
        return new Package();
    }

    /**
     * Create an instance of {@link GetAllPackagesForUsernameRequest }
     * 
     */
    public GetAllPackagesForUsernameRequest createGetAllPackagesForUsernameRequest() {
        return new GetAllPackagesForUsernameRequest();
    }

    /**
     * Create an instance of {@link GetAllPackagesForUsernameResponse }
     * 
     */
    public GetAllPackagesForUsernameResponse createGetAllPackagesForUsernameResponse() {
        return new GetAllPackagesForUsernameResponse();
    }

    /**
     * Create an instance of {@link GetAllPackagesRequest }
     * 
     */
    public GetAllPackagesRequest createGetAllPackagesRequest() {
        return new GetAllPackagesRequest();
    }

    /**
     * Create an instance of {@link GetAllPackagesResponse }
     * 
     */
    public GetAllPackagesResponse createGetAllPackagesResponse() {
        return new GetAllPackagesResponse();
    }

    /**
     * Create an instance of {@link SavePackageRequest }
     * 
     */
    public SavePackageRequest createSavePackageRequest() {
        return new SavePackageRequest();
    }

    /**
     * Create an instance of {@link DeletePackageRequest }
     * 
     */
    public DeletePackageRequest createDeletePackageRequest() {
        return new DeletePackageRequest();
    }

    /**
     * Create an instance of {@link Route }
     * 
     */
    public Route createRoute() {
        return new Route();
    }

}
