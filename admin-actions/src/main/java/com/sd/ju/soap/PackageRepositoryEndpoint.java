package com.sd.ju.soap;

import com.sd.ju.repository.PackageRepository;
import com.sd.ju.wsdl.*;

import com.sd.ju.wsdl.Package;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class PackageRepositoryEndpoint {
    private static final String NAMESPACE_URI = "http://sd-project.com/soap";

    private final PackageRepository packageRepository;

    @Autowired
    public PackageRepositoryEndpoint(PackageRepository packageRepository) {
        this.packageRepository = packageRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getPackageRequest")
    @ResponsePayload
    public GetPackageResponse getPackage(@RequestPayload GetPackageRequest request) {
        GetPackageResponse response = new GetPackageResponse();

        response.setPackage(packageRepository.findByName(request.getPackageName()));

        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "savePackageRequest")
    public void savePackage(@RequestPayload SavePackageRequest request) {
        packageRepository.save(request.getPackage());
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "deletePackageRequest")
    public void deletePackage(@RequestPayload DeletePackageRequest request) {
        Package packageToDelete = packageRepository.findByName(request.getPackageName());

        packageRepository.delete(packageToDelete);
    }
}
